<?php
// $Id$

/**
 * @file
 * node steps wrapper file
 */
?>
<div id="node-<?php print $node->nid; ?>" class="node clear-block node-steps
<?php
  if ($sticky) {
    print ' sticky';
  }
  if (!$status) {
    print ' node-unpublished';
  }
?>
">

  <?php print $picture ?>

  <?php if (!$page): ?>
  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  <?php endif; ?>

  <div class="meta">
    <?php if ($submitted): ?>
    <span class="submitted"><?php print $submitted ?></span>
    <?php endif; ?>

    <?php if ($terms): ?>
    <div class="terms terms-inline"><?php print $terms ?></div>
    <?php endif;?>
  </div>

  <div class="content node-step">
    <?php print $content ?>
  </div>

  <?php if (!empty($step_form)) { ?>
  <div class="steps-step-form">
    <?php print $step_form; ?>
  </div>
  <?php } ?>

  <?php print $links; ?>
</div>
